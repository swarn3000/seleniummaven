package Steps;

import PageObjects.LoginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LSteps {

    private WebDriver driver = null;

    @Given("^I am on the user form page$")
    public void i_am_on_the_user_form_page() throws Throwable {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Swarn\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to("https://google.com");
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @When("^I fill firstname \"([^\"]*)\" and fill the middlename \"([^\"]*)\" in the fields$")
    public void i_fill_firstname_and_fill_the_middlename_in_the_fields(String arg1, String arg2) throws Throwable {
        LoginPage page = new LoginPage(driver);
        page.text(arg2,arg1);
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @Then("^I close browser$")
    public void i_close_browser() throws Throwable {
        driver.close();
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

}
